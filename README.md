# CodeFuck Version 1.0
Le langage CodeFuck est une variante du BrainFuck, enrichie. Son développement a débuté en Décembre 2016. Ce langage propose une simplification de la syntaxe du BrainFuck en évitant les répétitions et en ajoutant de nouveaux opérateurs. Un code BrainFuck est 100% compatible avec L'interpreteur CodeFuck.

# Utilisation

## Compile CF2C (CodeFuck to C)
```
gcc -c "cf2c.c" -o "cf2c.o" -Werror -static
gcc "cf2c.o" -o "cf2c" -Werror -static
rm "cf2c.o"
```
## Compile hello.cf
```
./cf2c -f "examples/hello.cf" > "examples/hello.c"
gcc -c "examples/hello.c" -o "examples/hello.o" -Werror -static
gcc "examples/hello.o" -o "hello" -Werror -static
rm "examples/hello.o"
```
or
```
cat "examples/hello.cf" | ./cf2c > "examples/hello.c"
gcc -c "examples/hello.c" -o "examples/hello.o" -Werror -static
gcc "examples/hello.o" -o "hello" -Werror -static
rm "examples/hello.o"
```

## Compile CF2C, compile examples script parameter and run
```
./gcf examples/hello.cf
```
```
./gcf examples/input.cf
```
```
./gcf examples/math.cf
```

le script "gcf" compile le traducteur CodeFuck "cf2c" ensuite il convertit l'exemple CodeFuck en C. 

Pour finir il compile le C en binaire et l'exécute. GCC est nécessaire.
```
sudo apt-get install gcc
```

## CodeFuck Hello World
```
+10[>+7>+10>+3>+<4-]>+2.>+.+7.2+3.>+2.<2+15.>.+3.-6.-8.>+.>.
```
## BrainFuck Hello World
```
++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.
```

# Syntaxe
## Operator
Operator  | Description  
----------| -------------  
>         | Inc Cell  
<         | Dec Cell  
+         | Inc Octet  
-         | Dec Octet  
.         | Display Octet  
;         | Display Int Octet  
,         | Input  
:         | Input Int  
"Strings" | Display Strings Char
_         | load variable = octet
$         | display variable
f         | create function
F         | run function
[         | while ( octet != 0 ) {  
[x        | while ( octet != (int)x ) {  
/         | while ( octet == 0 ) {  
/x        | while ( octet == (int)x ) {  
(         | if ( octet == 0 ) {  
(x        | if ( octet == (int)x ) {  
{         | Start If ( octet != 0 ) {  
{x        | if ( octet != (int)x ) {  
!         | if ( octet > 0 ) {  
!x        | if ( octet > (int)x ) {  
?         | if ( octet < 0 ) {  
?x        | if ( octet < (int)x ) {  
&         | } else {  
&#124;    | } else ...  
**#** and **]** and **\\** and **}** and **)**         | End Block While/If/Else If/Else }  
%comment% | Comment /* your comment */...  

## Condition
### CodeFuck
```
(.|!.|?.)
```
### C
```
if (*cell==0) {
    putchar(*cell);
} else if (*cell>0) {
    putchar(*cell);
} else if (*cell<0) {
    putchar(*cell);
}
```
    
### CodeFuck
```
{100.|?50.&.}
```
### C
```
if (*cell!=100) {
    putchar(*cell);
} else if (*cell<50) {
    putchar(*cell);
} else {
    putchar(*cell);
}
```
    
### CodeFuck
```
[100{80.|!30.|?30.&.}+]
```
### C
```
while (*cell!=100) {
    if (*cell!=80) {
        putchar(*cell);
    } else if (*cell>30) {
        putchar(*cell);
    } else if (*cell<30) {
        putchar(*cell);
    } else {
        putchar(*cell);
    }
    ++*cell;
}
```

## Functions  

The creation of a function always takes place before any other operations  
  
### CodeFuck
```
f1
    +65.
f
.
F1
```
or  
```
f1+65.f.F1
```

### C
```
long f1(long cell) {
    [...]
    *cell+=65;
    putchar(*cell);
    return cell*;
}

int main(int argc, char **argv)
{
    [...]
    putchar(*cell);
    *cell=f1(*cell);
    [...]
}
```
    
## Example hello.cf
### CodeFuck
```
+10[
    >+7
    >+10
    >+3
    >+
    <4-
]
>+2.
>+.
+7.2
+3.
>+2.
<2+15.
>.
+3.
-6.
-8.
>+.
>.
"Salut le monde
"
>5
+48;" == "."
"
[-]
+65;" == "."
"
[-]
+97;" == "."
"
[-];" "
/+5\;" "
{-5};" "
(+);" "-;"
*cell ";" == ";"
"
[15+]
(15"*cell ";" == ";"
")
/15-\
{15"*cell ";" != 15"_}"
*cell ";" == "($;)[-]_"
"
```
### Output
```
Hello World!
Salut le monde!
48 == 0
65 == A
97 == a
05010
*cell 0 == 0
*cell 15 == 15
*cell 14 not 15
14 == 14
```
