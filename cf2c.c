// Copyright © 2016 David Lhoumaud. See AUTHORS.txt for details
// Licensed under the terms of the GPL v3. See licenses/GPL-3.txt

#include <stdio.h>
#include <stdlib.h>

int fout(long c, long s, long number, long block, long func);
long str2int(char* chaine1);
void memsetcf(char *array, int z, long unsigned int s );
void indent(long block);
const char * r_indent(long block);
int main(int argc, char *argv[]);

int fout(long c, long s, long number, long block, long func) {
    FILE *out = stdout;
    if (s==0 ) indent(block);
    switch (c) {
        case '>':
            if (s==0 && number==0) fprintf(out, "    ++cell;\n"); 
            else if (s==0 && number>0) fprintf(out, "    cell+=%li;\n", number);
            else if (s==0 ) fprintf(out, "    cell+=var;\n");
        break;
        case '<': 
            if (s==0 && number==0) fprintf(out, "    --cell;\n"); 
            else if (s==0 && number>0) fprintf(out, "    cell-=%li;\n", number);
            else if (s==0 ) fprintf(out, "    cell-=var;\n");
        break;
        case '+': 
            if (s==0 && number==0) fprintf(out, "    ++*cell;\n"); 
            else if (s==0 && number>0) fprintf(out, "    *cell+=%li;\n", number);
            else if (s==0 ) fprintf(out, "    *cell+=var;\n");
        break;
        case '-':
            if (s==0 && number==0) fprintf(out, "    --*cell;\n"); 
            else if (s==0 && number>0) fprintf(out, "    *cell-=%li;\n", number);
            else if (s==0 ) fprintf(out, "    *cell-=var;\n");
        break;
        case '.':
            if (s==0 && number==0) fprintf(out, "    putchar(*cell);\n"); 
            else {
                if (s==0) fprintf(out, "    putchar(*cell);");
                else fprintf(out, " putchar(*cell);");
                if (s==number-1) fprintf(out, "\n");
            } 
        break;
        case '[':
            if (s==0 && number>-1) {
                fprintf(out, "while (*cell!=%li) {\n", number );
            } else if (s==0) {
                fprintf(out, "while (*cell!=var) {\n");
            }
        break;
        case '/':
            if (s==0 && number>-1) {
                fprintf(out, "while (*cell==%li) {\n", number );
            } else if (s==0) {
                fprintf(out, "while (*cell==var) {\n");
            }
        break;
        case '(':
            if (s==0 && number>-1) {
                fprintf(out, "if (*cell==%li) {\n", number );
            } else if (s==0) {
                fprintf(out, "if (*cell==var) {\n");
            }
        break;
        case '{':
            if (s==0 && number>-1) {
                fprintf(out, "if (*cell!=%li) {\n", number );
            } else if (s==0) {
                fprintf(out, "if (*cell!=var) {\n");
            }
        break;
        case 'f':
            if (s==0 && number>-1 && func==1) {
                fprintf(out,"long f%li(long  cell_) {\n", number );
                fprintf(out,"    long *cell = calloc(30000, 1);\n"
                            "    cell=&cell_;\n"
                            "    long *cells = cell;\n"
                            "    char num[8]; char c; int i=0; int var=0;\n"
                            "    if (!cell) {\n"
                            "        fprintf(stderr, \"Error allocating memory.\\n\");\n"
                            "        return 1;\n"
                            "    }\n");
            } else if (s==0 && func==1) {
                fprintf(out, "long f(long  cell_) {\n");
                fprintf(out,"    long *cell = calloc(30000, 1);\n"
                            "    cell=&cell_;\n"
                            "    long *cells = cell;\n"
                            "    char num[8]; char c; int i=0; int var=0;\n"
                            "    if (!cell) {\n"
                            "        fprintf(stderr, \"Error allocating memory.\\n\");\n"
                            "        return 1;\n"
                            "    }\n");
            } else if (s==0 ) {
                if ( number>-1 )fprintf(out, "    *cell=f%li(*cell);\n", number );
                else fprintf(out, "    *cell=f(*cell);\n" );
            }
        break;
        case 'F':
            if (s==0 ) {
                if ( number>-1 )fprintf(out, "    *cell=f%li(*cell);\n", number );
                else fprintf(out, "    *cell=f(*cell);\n" );
            }
        break;
        case '!':
            if (s==0 && number>-1) {
                fprintf(out, "if (*cell>%li) {\n", number );
            } else if (s==0) {
                fprintf(out, "if (*cell>var) {\n");
            }
        break;
        case '?':
            if (s==0 && number>-1) {
                fprintf(out, "if (*cell<%li) {\n", number );
            } else if (s==0) {
                fprintf(out, "if (*cell<var) {\n");
            }
        break;
        case ';':
            if (s==0 && number==0) fprintf(out, "    printf(\"%s\", *cell);\n", "%li");
            else {
                if (s==0) fprintf(out, "    printf(\"%s\", *cell);", "%li");
                else fprintf(out, " printf(\"%s\", *cell);\n", "%li");
                if (s==number-1) fprintf(out, "\n");
            } 
        break;
    }
    return 0;
}

long str2int(char* chaine1) {
    long i=atoi(chaine1);
    return i;
}

void memsetcf(char *array, int z, long unsigned int s ) {
    long unsigned int p;
    for (p=z; p<=s; p++){
        array[p]=z;
    }
}

void indent(long block) {
    long o=0;
    while (o<block) {
        fprintf(stdout, "    ");
        o++;
    }
}

const char * r_indent(long block) {
    long o=0;
    static char a[30000];
    while (o<block) {
        a[o++]=' ';a[o++]=' ';
        a[o++]=' ';a[o++]=' ';
    }
    a[o]='\0';
    return a;
}

int egal(char* chaine1, const char* chaine2) {
    int i = 0;
    for ( i = 0; chaine1[i] != '\0'; i++) {
        if ( chaine1[i]!= chaine2[i]) {
            return -1;
        }
    }
    return 0;
}

void help() {
    fprintf(stderr, 
        "\033[1mCodeFuck\033[0m v1.0 by David Lhoumaud\n"
        "\033[33;1mUsage :\033[0m cf2c <option> or cat script.cf | cf2c\n"
        "\t\033[1m-f\033[0m\tfilename\n"
        "\t\033[1m-g\033[0m\tEnter your code in the terminal.\n\t\tTo finish the generation input the character @\n"
        "\t\033[1m-h\033[0m\tdisplay help\n"
        "\033[33;1mExamples :\033[0m\n"
        "\tcf2c -f examples/hello.cf > hello.c\n"
        "\tcat examples/hello.cf | cf2c  > hello.c\n"
        "\tcf2c -g > test.c\n\n"
    );
}

void gen_main() {
    fprintf(stdout,
        "int main(int argc, char **argv)\n{\n"
        "    long *cell = calloc(30000, 1);\n"
        "    long *cells = cell;\n"
        "    char num[8]; char c; int i=0; int var=0;\n"
        "    if (!cell) {\n"
        "        fprintf(stderr, \"Error allocating memory.\\n\");\n"
        "        return 1;\n"
        "    }\n"
        "/*--------------------------------------------*/\n"
        "/*------------------CodeFuck------------------*/\n"
        "/*--------------------------------------------*/\n"
    );
}

int main(int argc, char *argv[]) {
    FILE *in = stdin, *out = stdout;
    long c, block=0, oblock=0;
    int elseif=1, GEN=-1;
    if (argc>1 ) {
        if (egal(argv[1],"-h")==0) {
            help();
            return 0;
        } else if (egal(argv[1],"-f")==0) {
            in=fopen(argv[2],"r");
            if (in == NULL) {help();fprintf(stderr, "\033[31;1m[ERROR]\033[0m \033[31mFile not found !\033[0m\n");return 1;}
        } else if (egal(argv[1],"-g")==0) {
            GEN=0;
        }
    }
    
    fprintf(out,
        "#include <stdio.h>\n"
        "#include <stdlib.h>\n\n"
        "void memsetcf(char *array, int z, long unsigned int s );\n"
        "int main(int argc, char **argv);\n\n"
        "void memsetcf(char *array, int z, long unsigned int s ) {\n"
        "   long unsigned int p;\n"
        "   for (p=z; p<=s; p++){\n"
        "   array[p]=z;\n"
        "   }\n"
        "}\n\n"
    );
    
    char num[8];
    long old, cpt=0, s=0, ch=1, com=1, isfunction=2; 
    
    if (GEN==0) fprintf(stderr,"\033[33m");
    
    while ((c = getc(in)) != EOF) {
        if (elseif==3) { elseif=1;block=oblock; }
        else if (elseif==2) { elseif=3; }
        
        if (GEN==0) fprintf(stderr,"\033[0m");
        if (c=='@' && GEN==0)break;
        if (com==0 && c!='%') {
            putchar(c);
            continue;
        }
        if (ch==0 && c!='"') {
            indent(block);
            fprintf(out, "    putchar(%li);\n", c); 
            continue;
        }
        switch (c) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if (isfunction==2) {gen_main();isfunction=3;}
                if (elseif==3)elseif=2;
                if (old>0) num[cpt++]=c; 
            break;
            case '%':
            case '_': 
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    while (s < str2int(num)) {
                        fout(old, s, str2int(num), block, isfunction);
                        s++;
                    }
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                if (c=='_') {indent(block);fprintf(out, "    var=*cell;\n");}
                else if(c=='%') {
                    if (com==1) {
                        com=0;
                        fprintf(out, "/*");
                    } else {
                         com=1;
                         fprintf(out, "*/\n");
                    }
                }
            break;
            case '$':
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0) {
                    if (elseif==3)elseif=2;
                    fout(old, s, -1, block, isfunction);
                    s=0;
                    cpt=0;
                    memsetcf (num, 0, sizeof (num));
                    old=0;
                } else if (ch==0) {
                    indent(block);
                    fprintf(out, "    putchar(%li);\n", c);
                }
            break;
            case '>':
            case '<':
            case '+': 
            case '-':
            case 'F':
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    else fout(old, s, str2int(num), block, isfunction);
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                old=c;
            break;
            case '.'://display ASCII
            case ';'://display INTEGER
            case ','://input ASCII
            case ':'://input INTEGER
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    while (s < str2int(num)) {
                        fout(old, s, str2int(num), block, isfunction);
                        s++;
                    }
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                
                if (c==':') {
                    fprintf(out, 
                        "%s    *cell = getchar();\n"
                        "%s    while (*cell!=0) {\n"
                        "%s        num[i]=*cell; i++;\n"
                        "%s        *cell-=10;\n"
                        "%s        if (*cell!=0) {\n"
                        "%s            *cell+=10;\n"
                        "%s            *cell = getchar();\n"
                        "%s        }\n"
                        "%s    }\n"
                        "%s    *cell = atoi(num);\n"
                        "%s    memsetcf(num, 0, sizeof (num)); i=0;\n"
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                        ,r_indent(block)
                    ); 
                } else if (c==','){
                    indent(block);
                    fprintf(out, "    *cell = getchar();\n"); 
                } else old=c;
            break;
            case '['://while octet != 0
            case '/'://while octet == 0
            case '{'://if octet != 0
            case '('://if octet == 0
            case '!'://if octet > 0
            case '?'://if octet < 0
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0 ) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    while (s < str2int(num)) {
                        fout(old, s, str2int(num), block, isfunction);
                        s++;
                    }
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                
                if (elseif==0) {
                    oblock=block;
                    block=0;
                    elseif=2;
                } else if (elseif==1){
                    block++;
                }
                old=c;
            break;
            case ']':
            case '\\':
            case '}':
            case ')':
            case '|':
            case '&':
            case '#':
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    while (s < str2int(num)) {
                        fout(old, s, str2int(num), block, isfunction);
                        s++;
                    }
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                if (c=='&') {indent(block);fprintf(out, "} else {\n"); }
                else if (c=='|') {indent(block);fprintf(out, "} else ");elseif=0; }
                else {
                    indent(block);
                    block--;
                    fprintf(out, "}\n"); 
                }
            break;
            case '"':
                if (isfunction==2) {gen_main();isfunction=3;}
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    while (s < str2int(num)) {
                        fout(old, s, str2int(num), block, isfunction);
                        s++;
                    }
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                if (ch==1) ch=0;
                else ch=1;
            break;
            case 'f':
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    else fout(old, s, str2int(num), block, isfunction);
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
                if (isfunction==2) {
                    old=c;
                    isfunction=1;
                } else if (isfunction==1) {
                    isfunction=2;
                    fprintf(out, "    return *cell;\n}\n\n"); 
                } else if (isfunction==3) {
                    old=c;
                }
            break;
            default: 
                if (old>0) {
                    if (str2int(num)==0) fout(old, 0, 0, block, isfunction);
                    while (s < str2int(num)) {
                        fout(old, s, str2int(num), block, isfunction);
                        s++;
                    }
                    s=0;
                    cpt=0;
                    old=0;
                    memsetcf (num, 0, sizeof (num));
                }
            break;
        }
        if (GEN==0) fprintf(stderr,"\033[33m");
    }
    if (GEN==0) fprintf(stderr,"\033[0m");
    fprintf(out, 
        "/*--------------------------------------------*/\n"
        "/*----------------End CodeFuck----------------*/\n"
        "/*--------------------------------------------*/\n"
        "    free(cells);\n    return 0;\n}\n");
}
